# %%
from pathlib import Path
import re
import json

import dateutil
import datetime
import ics

# %%
course = ics.Calendar(Path("timetable.ics").read_text())

events = []


def parse_data(description):
    type_conversion = {
        "Exam/Test in Osiris": "Exam",
        "Exam/Test in Osiris (Retake)": "Retake exam",
        "Written Exam with registration on campus": "Exam",
        "Written Test without registration": "Minitest",
        "Lecture": "Lecture",
        "Seminar": "Exercises",
        "Instruction": "Exercises",
    }
    type = type_conversion[
        re.search("^Type: (.*)$", description, flags=re.MULTILINE).group(1)
    ]
    out = re.search(
        "^Location\\(s\\):\n(?P<name>.*?)(?:: http:.*?)?$",
        description,
        flags=re.MULTILINE,
    ).groupdict()
    out["type"] = type
    return out

def mark_minitests(event):
    """
    Rename the event title to "Minitest".
    This function depends on the schedule and how mytimetable names the
    exams/minitests.
    """
    event_time = datetime.datetime.fromisoformat(event["start"])
    weekday = event_time.weekday()
    # Modify condition to match the timetable.
    if weekday == 0 and event["title"] == "Exam":
        event["title"] = "Minitest"
    return event

for original in course.events:
    parsed = parse_data(original.description)

    event = {
        "start": str(original.begin),
        "end": str(original.end),
        "title": f'{parsed["type"]}',
        "location": f'{parsed["name"]}',
    }
    mark_minitests(event)

    events.append(event)

events = sorted(events, key=(lambda event: dateutil.parser.isoparse(event["start"])))
last_lecture = False
for event in events:
    if event["title"] == "Lecture":
        last_lecture = True
    if event["title"] == "Exercises":
        if last_lecture:
            last_lecture = False
        else:
            event["title"] = "Q&A"

events[-1]["title"] = events[-1]["title"].replace("Exam", "Retake")
Path("timetable.json").write_text(json.dumps(events))

# %%
