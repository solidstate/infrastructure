# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
import os
from io import BytesIO

import pandas
import numpy as np
import requests
import browser_cookie3

zesje_base_url = os.getenv("ZESJE_URL")

cookies = browser_cookie3.chrome()


def api_query(path):
    return requests.get(zesje_base_url + "/api/" + path, cookies=cookies)


all_data = {
    exam["name"]: pandas.read_pickle(
        BytesIO(api_query(f'export/dataframe/{exam["id"]}').content)
    )
    for exam in api_query("exams").json()
}

### Check that all solutions have positive scores

for test, data in all_data.items():
    problem_scores = data.xs("total", axis=1, level=1, drop_level=True)

    print(test, problem_scores[problem_scores < 0].stack().index.tolist())

((all_data["Minitest 2"]["total", "total"] / 80 * 9 + 1) > 5.75).mean()

minitest2 = all_data["Minitest 2"]
# -

minitest2[
    (5 < minitest2["total", "total"] / 80 * 9 + 1)
    & (5.75 > minitest2["total", "total"] / 80 * 9 + 1)
][["Problem 2c", "Problem 2d"]].dropna(axis=1, how="all")

# ## Combining grades

# +
totals = pandas.DataFrame({k: v[("total", "total")] for k, v in all_data.items()})

totals["Waiver number"] = 1

# Students with a minitest waiver:
totals.loc[[], "Waiver number"] = 2
# Following the exam committee instructions
totals.loc[[], "retake"] = np.nan

# Only select those that took the final or retake
totals = totals[totals["Final"].notna() | totals["retake"].notna()].fillna(0)


def rescale(offset=1, coefficient=0.09):
    return lambda score: min(offset + coefficient * score, 10)


totals["Minitest 1"] = totals["Minitest 1"].apply(rescale(1, coefficient=0.1))
totals["Minitest 2"] = totals["Minitest 2"].apply(rescale())
totals["Minitest 3"] = totals["Minitest 3"].apply(rescale())
totals["Final"] = totals["Final"].apply(rescale(1, 0.09 / 1.3))
totals["retake"] = totals["retake"].apply(rescale(1, 0.09 / 1.4))
# totals['Retake'] = totals['Retake'].apply(rescale(1, 0.09/1.5))


def grade(student):
    minitest_names = ["Minitest 1", "Minitest 2", "Minitest 3"]
    minitests = np.sort(student[minitest_names].values)
    final = student["Final"]
    waiver = int(student["Waiver number"])
    minitests[:waiver][minitests[:waiver] < final] = final
    grade = 0.1 * sum(minitests) + (1 - 0.1 * len(minitest_names)) * final
    return grade


def grade_retake(student):
    minitest_names = ["Minitest 1", "Minitest 2", "Minitest 3"]
    minitests = np.sort(student[minitest_names].values)[:2]
    retake = student["retake"]
    minitests[minitests < retake] = retake
    grade = 0.1 * sum(minitests) + (1 - 0.1 * len(minitests)) * retake
    return grade


totals["Grade"] = totals.apply(grade, axis=1)
totals["retake_grade"] = totals.apply(grade_retake, axis=1)
# -

# %matplotlib inline

retake_grade = totals[totals.retake > 1].retake_grade.apply(lambda x: round(x * 2) / 2)

totals.describe()

# +
# Passing due to minitests

totals[(totals.Grade > 5.75) & (totals.Final < 5.75)].index.values

# +
# Failing due to minitests

totals[(totals.Grade < 5.75) & (totals.Final > 5.75)].index.values
# -

totals.Grade[totals.Grade.apply(lambda x: 5.5 < x < 5.75)]

totals[totals.Grade.apply(lambda x: round(x * 2) / 2) >= 9]

totals.Grade.apply(lambda x: round(x * 2) / 2).hist(bins=30)

totals.Final.hist(bins=40)

(totals.Grade.apply(lambda x: round(x * 2) / 2) >= 6).mean()

# ## Analysis

ax = totals["Grade"].hist(bins=20, rwidth=0.5)
ax.set_xlim(0, 10)

totals["old_grade"] = old_totals["Grade"]

totals.describe()

# # Save pass/fail file in the format expected by Osiris

pandas.DataFrame(totals["Grade"].apply(lambda x: "V" if x > 5.75 else "O")).to_csv(
    "grades.txt", header=False, sep=" "
)

# ## Same, but with grades

pandas.DataFrame(totals["Grade"].apply(lambda x: round(2 * x) / 2)).to_csv(
    "grades.txt", header=False, sep=" "
)

pandas.DataFrame(retake_grade).to_csv("grades.txt", header=False, sep=" ")
