# %%
from pathlib import Path
import re

from requests import get, post, patch, delete
import browser_cookie3
from ruamel.yaml import YAML

# %%
yaml = YAML()
yaml.default_flow_style = False
yaml.sort_keys = False
# %%
course_data = yaml.load(Path(__file__).parent.parent / "config.yml")
zesje_base_url = course_data["zesje_url"]
cookies = browser_cookie3.chrome()
exams_dir = Path(__file__).parent.parent / "exams"
problems_dir = Path(__file__).parent.parent / "Problems"
email_template = (Path(__file__).parent / "email_template.jinja2").read_text()


# %%
def api_query(method, endpoint, *, raise_for_status=True, **kwargs):
    """Make an API query to Focalboard

    Parameters
    ----------
    method : callable
        The method to use (requests.get, requests.post, etc.)
    endpoint : str
        The endpoint to query
    raise_for_status : bool, optional
        Whether to raise an exception if the status code is >300, by default True
    notify : bool, optional
        Whether to notify users of the change, by default False
    **kwargs : dict
        Additional keyword arguments to pass to the method

    Returns
    -------
    dict
        The JSON response
    """
    response = method(
        zesje_base_url + "api/" + endpoint,
        cookies=cookies,
        **kwargs,
    )
    if raise_for_status:
        response.raise_for_status()
    try:
        return response.json()
    except ValueError:
        return response


def subquestions_in_zesje_exam(exam_name):
    """
    Get a list of zesje subquestions from an exam.
    exam_name: str

    Returns:
        list of dictionaries for each subquestion in the exam.
    """
    exams = api_query(get, "exams")
    exam_id = [exam["id"] for exam in exams if exam["name"] == exam_name][0]
    return api_query(get, f"exams/{exam_id}")["problems"]


def create_template(exam_name):
    """
    Create a template for feedback for an exam.

    exam_name: str
    """
    exam = next(exam for exam in course_data["exams"] if exam["name"] == exam_name)
    problems = subquestions_per_problem(exam)
    for problem_path, subquestions in problems.items():
        content = [{"score": points, "feedback": None} for points in subquestions]
        filename = problems_dir / f"{problem_path}.yml"
        with open(filename, "w") as file:
            yaml.dump(content, file)


def post_exam_feedback(exam_name):
    """
    Post feedback for an exam using a template with the same name.

    exam_name: str
    """
    # Find the exam in the course data.
    exam = next(exam for exam in course_data["exams"] if exam["name"] == exam_name)

    # Get the problems from the tex file.
    problems = subquestions_per_problem(exam)

    # Make 3 lists: problem paths, subquestions in zesje, and subquestion names.
    problem_paths = [
        path
        for path, subquestions in problems.items()
        for _ in range(len(subquestions))
    ]
    subquestions_zesje = subquestions_in_zesje_exam(exam_name)
    # Standarize all subquestion names to "i (a)", "i (b)", etc.
    subquestions_names = [
        f"{i + 1} ({chr(97 + j)})"
        for i, (_, subquestions) in enumerate(problems.items())
        for j in range(len(subquestions))
    ]

    # Post feedback for each subquestion.
    for subquestion, problem_path, subquestion_name in zip(
        subquestions_zesje, problem_paths, subquestions_names
    ):
        with open(problems_dir / f"{problem_path}.yml", "r") as file:
            subquestion_feedback = yaml.load(file)[ord(subquestion_name.split('(')[1][0]) - 97]
        # update subquestion name
        api_query(
            patch, f"problems/{subquestion['id']}", json={"name": subquestion_name}
        )
        # delete existing feedback
        root_feedback = subquestion["feedback"][str(subquestion["root_feedback_id"])]
        for child_id in root_feedback["children"]:
            if subquestion["feedback"][str(child_id)]["name"] != "Blank":
                api_query(delete, f"feedback/{subquestion['id']}/{child_id}")
        # post feedback
        post_subquestion_feedback(
            subquestion_feedback, subquestion["id"], subquestion["root_feedback_id"]
        )
        api_query(
            patch,
            f"feedback/{subquestion['id']}/{subquestion['root_feedback_id']}",
            json={"exclusive": True},
        )
        # TODO: update the email template, a 422 error appears when executing these lines
        # exams = api_query(get, "exams")
        # exam_id = next(exam["id"] for exam in exams if exam["name"] == exam_name)
        # api_query(put, f"templates/{exam_id}", json={"template": email_template})


def post_subquestion_feedback(subquestion_feedback, problem_id, parent_id):
    """
    Post feedback for a subquestion.

    feedback: list of dictionaries, where each dictionary is a feedback option.
    problem_id: int
    parent_id: int
    """
    default_feedback = [
        {
            "name": "Math error",
            "score": -1,
        },
        {
            "name": "Dimensionality error",
            "score": -2,
        },
        {
            "name": "Extra space",
            "score": 0,
        },
    ]
    feedback = default_feedback + subquestion_feedback.get("feedback", [])
    score = subquestion_feedback.get("score", 0)
    full_feedback = [
        {
            "name": "Blank",
            "score": 0,
        },
        {
            "name": "Incorrect solution",
            "score": 0,
        },
        {
            "name": "Fully/partially correct solution",
            "score": score,
            "suboptions": feedback,
        },
    ]
    for item in full_feedback:
        item = {**item, "parentId": parent_id}
        posted = api_query(post, f"feedback/{problem_id}", json={"name": item["name"], "score": item["score"], "parentId": item["parentId"], "description": item.get("description", "")})
        post_suboptions_recursively(
            problem_id, posted["id"], item.get("suboptions", [])
        )
        api_query(
            patch,
            f"feedback/{problem_id}/{parent_id}",
            json={"exclusive": item.get("exclusive", False)},
        )


def post_suboptions_recursively(problem_id, parent_id, suboptions=None):
    """
    Post feedback suboptions recursively.

    problem_id: int
    parent_id: int
    suboptions: list of dictionaries, where each dictionary is a feedback suboption.
    """
    for option in suboptions:
        option = {**option, "parentId": parent_id}
        posted = api_query(post, f"feedback/{problem_id}", json={"name": option["name"], "score": option["score"], "parentId": option["parentId"], "description": option.get("description", "")})
        post_suboptions_recursively(
            problem_id, posted["id"], option.get("suboptions", [])
        )
        api_query(
            patch,
            f"feedback/{problem_id}/{parent_id}",
            json={"exclusive": option.get("exclusive", False)},
        )


def process_feedback(source_feedback, total_solutions):
    """
    Convert feedback from Zesje to store in the exams repo.

    source_feedback: dict
        The feedback from Zesje.
    total_solutions: int
        The total number of solutions for the problem.

    Returns:
        dict
            The processed feedback.
    """
    feedback_dictionaries = {
        feedback_option["id"]: {
            "name": feedback_option["name"],
            "score": feedback_option["score"],
            "description": feedback_option["description"],
            "exclusive": feedback_option["exclusive"],
            "frequency": round(feedback_option["used"] / total_solutions, 2),
            "suboptions": [],
        }
        for feedback_option in source_feedback.values()
    }
    for feedback_option in source_feedback.values():
        if feedback_option["parent"] is None:
            continue
        feedback_dictionaries[feedback_option["parent"]]["suboptions"].append(
            feedback_dictionaries[int(feedback_option["id"])]
        )
    # We don't care about the root, and only need its child that has children of its own
    # (i.e. the "Fully/partially correct solution" option).
    root_id = next(
        feedback["id"]
        for feedback in source_feedback.values()
        if not feedback["parent"]
    )
    feedback = next(
        option
        for option in feedback_dictionaries[root_id]["suboptions"]
        if option["suboptions"]
    )["suboptions"]

    for option in feedback_dictionaries.values():
        for entry in ["exclusive", "suboptions", "description"]:
            if not option[entry]:
                del option[entry]
        if option["name"] == "Dimensionality error":
            option["name"] = "Units error"
    return feedback


def subquestions_per_problem(exam):
    """Get the subquestions per problem in each exam."""
    exam_source = (exams_dir / exam["file"]).read_text()
    problem_names = re.findall(
        r"\\newcommand{\\problem(?:one|two|three)}{([^}]+?)}",
        exam_source,
    )
    problem_sources = [
        problems_dir / f"{problem_name}.tex" for problem_name in problem_names
    ]
    subquestions_per_problem = {
        problem_name: [
            int(i) for i in re.findall(r"\\part\[(\d+)\]", problem_source.read_text())
        ]
        for problem_name, problem_source in zip(problem_names, problem_sources)
    }
    return subquestions_per_problem


def save_year_data():
    """Download the complete data from a single year and save it in the exams repo."""
    # List all exams and problems in each exam.
    #
    # The problems are in \input command but don't have "ExtraAnswerSpace" in the
    # name. We need to replace "\ProblemsPath" with "problems", remove extension if
    # present and add .yml extension.

    problems_per_exam = []
    for exam in course_data["exams"]:
        problems_per_exam.append(subquestions_per_problem(exam))

    # Get all exam data from Zesje.
    exams = api_query(get, "exams")
    full_exam_data = [
        api_query(get, f"exams/{exam['id']}") for exam in exams
    ]  # this takes a bit of time

    # The feedback is a dictionary with keys being the feedback ids, and each
    # storing children ids in the "children" key. We all children to be in the
    # "suboptions" key, so we need to recursively traverse the tree.
    processed_data = {}
    for exam, source in zip(full_exam_data, problems_per_exam):
        problems = iter(exam["problems"])
        for problem_name, scores in source.items():
            processed_data[problem_name] = [
                dict(
                    score=score,
                    feedback=process_feedback(
                        next(problems)["feedback"], len(exam["submissions"])
                    ),
                )
                for score in scores
            ]
    # Create a "notes" key for each problem, which stores:
    # - The frequency of "Extra space" feedback.
    # - The **joint** frequency of "Math error" and "Units error" feedback.
    # - Grader notes.
    #
    # The corresponding feedback should be removed from the "feedback" key.
    for data in processed_data.values():
        for item in data:
            item["notes"] = dict(
                math_units_errors=0,
                remarks="",
                extra_space=0,
            )
            for feedback in item["feedback"]:
                if feedback["name"] == "Extra space":
                    item["notes"]["extra_space"] = feedback["frequency"]
                elif feedback["name"] == "Math error":
                    item["notes"]["math_units_errors"] += feedback["frequency"]
                elif feedback["name"] == "Units error":
                    item["notes"]["math_units_errors"] += feedback["frequency"]

            item["feedback"] = [
                feedback
                for feedback in item["feedback"]
                if feedback["name"] not in ["Extra space", "Math error", "Units error"]
            ]

    # Save the data.
    for problem_name, data in processed_data.items():
        with open(problems_dir / f"{problem_name}.yml", "w") as f:
            yaml.dump(data, f)


# %%
