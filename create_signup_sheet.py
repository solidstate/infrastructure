# %%
import json
import datetime
from string import ascii_uppercase as abc
from pathlib import Path

import xlsxwriter
from ruamel.yaml import YAML
# %%

# Parameters
yaml = YAML()
with Path("course.yml").open() as f:
    course = yaml.load(f)
num_Pis = len(course["team"]["instructors"])
num_TAs = len(course["team"]["tas"])
people = course["team"]["instructors"] + course["team"]["tas"]
activities = course["activities"]

with Path("timetable.json").open() as f:
    # Output from convert_timetable.py
    schedule = json.load(f)
first_week = datetime.datetime.fromisoformat(schedule[0]["start"]).isocalendar()[1]

# Create a workbook and add a worksheet.
with xlsxwriter.Workbook("solid_state_schedule.xlsx") as workbook:
    worksheet = workbook.add_worksheet()

    # Formats
    bold = workbook.add_format({"bold": True})
    red_format = workbook.add_format({"bg_color": "#FFC7CE"})
    green_format = workbook.add_format({"bg_color": "#C6EFCE"})
    yellow_format = workbook.add_format({"bg_color": "#FFEB9C"})

    # Conditional formatting
    def format_row(i, num_Pis_needed, num_TAs_needed):
        i += 1
        criteria_PIs = (
            f"=COUNTIF(${abc[2]}" + f"${i}" + f":${abc[1+num_Pis]}" + f"${i}, TRUE)"
        )
        criteria_TAs = (
            f"=COUNTIF(${abc[2+num_Pis]}"
            + f"${i}"
            + f":${abc[1+len(people)]}"
            + f"${i}, TRUE)"
        )

        formats = [red_format, green_format, yellow_format]
        relations = ["<", "=", ">"]

        for relation, format in zip(relations, formats):
            worksheet.conditional_format(
                f"${abc[2]}" + f"${i}" + f":${abc[1+num_Pis]}" + f"${i}",
                options={
                    "type": "formula",
                    "criteria": criteria_PIs + f"{relation}{num_Pis_needed}",
                    "format": format,
                },
            )

            worksheet.conditional_format(
                f"${abc[2+num_Pis]}" + f"${i}" + f":${abc[1+len(people)]}" + f"${i})",
                options={
                    "type": "formula",
                    "criteria": criteria_TAs + f"{relation}{num_TAs_needed}",
                    "format": format,
                },
            )

    # Write dates
    row, col = 3, 0
    last_week = 0
    for event in schedule:
        event_time = datetime.datetime.fromisoformat(event["start"])
        week = event_time.isocalendar()[1] - first_week + 1

        if week != last_week:
            worksheet.write(row, col, f"Week {week}", bold)
            row += 1

        worksheet.write(row, col + 1, f"{event_time:%A %-d/%-m}")
        worksheet.write(row, col, event["title"])
        format_row(row, *activities[event["title"]].values())

        last_week = week
        row += 1

    # Write headers
    worksheet.write(0, 0, "Activity", bold)
    worksheet.write(0, 1, "Date", bold)
    for i, name in enumerate(people):
        worksheet.write(0, i + 2, name, bold)
        worksheet.write_formula(
            1, i + 2, f"=COUNTIF(${abc[i+2]}$4:${abc[i+2]}${row}, TRUE)"
        )
    worksheet.write(0, len(people) + 3, "Student attendance", bold)

    # Formatting
    worksheet.set_column(0, len(people) + 1, 14)
    worksheet.set_column(len(people) + 3, len(people) + 2, 7)
    worksheet.set_column(len(people) + 3, len(people) + 3, 20)
# %%
