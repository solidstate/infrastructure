from getpass import getpass
import os
import pydiscourse

# Get a global/all permissions key at https://forum.solidstate.quantumtinkerer.tudelft.nl/admin/api/keys/new

token = os.getenv("DISCOURSE_TOKEN")
discourse_client = pydiscourse.DiscourseClient(
    host="https://forum.solidstate.quantumtinkerer.tudelft.nl/",
    api_username="anton-akhmerov",
    api_key=(token or getpass()),
)

# Add all team members to the course group as owners
team_members = [user["username"] for user in discourse_client.group_members("team")]
course_id = discourse_client.group("course")["group"]["id"]
discourse_client._put(
    f"/groups/{course_id}/owners.json", **{"usernames": ",".join(team_members)}
)
