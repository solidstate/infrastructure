# %%
import re
import os
from datetime import datetime, timedelta, timezone
from pathlib import Path
import json
from jinja2 import Template

import pydiscourse

# %%
event_markdown = Template(
    """[event start="{{start}}" end="{{end}}" status="standalone" name="{{title}} (at {{location}})" timezone="Europe/Amsterdam" minimal="true"]
[/event]
"""
)


def render_announcement(body, title, event):
    # Skip the room number and URL for now
    location = event["location"].split(" (")[0]
    data = {**event, "title": title, "location": location}
    return (
        Template(body).render(data)
        + "\n\n"
        + event_markdown.render(data)
    )



# Read the course schedule from mytimetable export (converted to json by convert_timetable.py)
schedule = json.load(Path("timetable.json").open())
for event in schedule:
    event["start"] = datetime.fromisoformat(event["start"])
    event["end"] = datetime.fromisoformat(event["end"])
    event["type"] = event.pop("title")
schedule = sorted(schedule, key=lambda event: event["start"])

# Read the file with the list of draft announcements
drafts = Path("announcements.md").read_text()
# Announcements are separated by a markdown h1 header with the announcement title
# The title is used as the topic title
# The text after the title is used as the topic body
# We use a regex to extract the title and the body

# TODO: The regex needs a h1 at the end; need to fix matching the last announcement.
drafts = re.findall(
    r"#\s+(?P<title>.*?)\n\s*(?P<body>.*?)\n+(?=#\s+)", drafts, flags=re.DOTALL
)
# Match the announcements to the schedule
# - The announcements titles contain "Lecture" / "Q&A" / "Minitest" / "Exam"
#   / "Retake exam" or the same with "result"
drafts_by_type = {
    "Lecture": (draft for draft in drafts if "Lecture" in draft[0]),
    "Q&A": (draft for draft in drafts if "Q&A" in draft[0]),
    "Minitest": (draft for draft in drafts if "Minitest" in draft[0]),
    "Exam": (draft for draft in drafts if "exam" in draft[0]),
    "Retake": (draft for draft in drafts if "Retake" in draft[0]),
}
announcements = [
    (event, *drafts_by_type[event["type"]].__next__())
    for event in schedule
    if event["type"] in drafts_by_type
]

# Determine the time of the announcement.
# - For lectures or Q&A sessions, the announcement is posted at the end of
#   the working day of the previous activity (lecture, minitest, or Q&A)
# - For minitests and exams, the announcement is posted 5 days in advance
times = []
for i, (event, *_) in enumerate(announcements):
    if event["type"] in ("Lecture", "Q&A"):
        if not i:
            # First lecture
            times.append(event["start"] - timedelta(days=1))
        else:
            # Post at the end of the working day of the previous activity
            times.append(
                announcements[i - 1][0]["end"].replace(hour=17, minute=0)
            )
    elif event["type"] in ("Minitest", "Exam", "Retake"):
        # Post 5 days in advance
        times.append(event["start"] - timedelta(days=5))
    else:
        raise ValueError(f"Unknown event type {event['type']}")

# # Test how everything looks
# Path("announcements.txt").write_text(
#     "\n\n".join(
#         f"{event['type']} {event['start']}\n{title}\n{render_announcement(body, title, event)}"
#         for event, title, body in announcements
#     )
# )

# Create a new topic for each draft announcement
# - Set the title to the title of the draft announcement
# - Add the discource "event" markup with the activity time

# Get a global/all permissions key at https://forum.solidstate.quantumtinkerer.tudelft.nl/admin/api/keys/new

# # Get the token from pass if you are using it.
# from subprocess import check_output
# os.environ["DISCOURSE_TOKEN"] = check_output("pass solidstate_forum_token", shell=True).decode().strip()

token = os.getenv("DISCOURSE_TOKEN")
discourse_client = pydiscourse.DiscourseClient(
    host="https://forum.solidstate.quantumtinkerer.tudelft.nl/",
    api_username="anton-akhmerov",
    api_key=token,
)

announcement_ids = []
for (event, title, body), time in zip(announcements, times):
    if time < datetime.now(tz=timezone.utc):
        # Skip announcements that are in the past
        continue
    post = discourse_client.create_post(
        content=render_announcement(body, title, event),
        title=title,
        category_id=8,
    )
    discourse_client._post(
        f"/t/{post['topic_id']}/timer",
        **{
            "time": time.isoformat(),
            "status_type": "publish_to_category",
            "category_id": 17,
        },
        json=True,
    )
    announcement_ids.append(post["topic_id"])

# # Delete all announcements, useful for testing
# for announcement_id in announcement_ids:
#     discourse_client.delete_topic(announcement_id)
# %%
